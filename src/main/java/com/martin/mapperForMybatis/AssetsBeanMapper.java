package com.martin.mapperForMybatis;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.martin.model.bean.AssetsBean;

@Mapper
public interface AssetsBeanMapper {

	/**
	 * 尋找所有資產
	 * @return 資產或者沒有東西
	 */
	@Select("select * from assets")
//	@Results({ @Result(property = "id", column = "id"), @Result(property = "builtDate", column = "built_date"),
//			@Result(property = "name", column = "name"), @Result(property = "unit", column = "unit"),
//			@Result(property = "user", column = "user"), @Result(property = "value", column = "value") })
	List<AssetsBean> findAllAssetsBean();

	/**
	 * 尋找特定編號的資產
	 * @param id 資產編號
	 * @return 特定編號的資產或者沒有東西
	 */
	@Select("select * from assets where id=#{id}")
//	@ResultMap("assetsBeanMapper")
	AssetsBean findAssetsBeanByAssetsBeanId(Integer id);

	/**
	 * 新增資產
	 * @param assetsBean 新增的資產
	 * @return 成功的筆數
	 */
	@Insert("insert into assets (built_date, name, unit, [user], value) values(#{builtDate}, #{name}, #{unit}, #{user}, #{value})")
	Integer createAssetsBean(AssetsBean assetsBean);

	/**
	 * 更新資產
	 * @param assetsBean 更新後資產
	 * @return 成功的筆數 
	 */
	@Update("update assets set built_date=#{builtDate}, name=#{name}, unit=#{unit}, [user]=#{user}, value=#{value} where id=#{id}")
	Integer updateAssetsBean(AssetsBean assetsBean);

	/**
	 * 刪除資產
	 * @param id 資產編號
	 * @return 成功的筆數
	 */
	@Delete("delete from assets where id=#{id}")
	Integer delete(Integer id);

	/**
	 * 尋找特定部門的資產
	 * @param unit
	 * @return 資產或者沒有東西
	 */
	@Select("select * from assets where unit=#{unit}")
//	@Results(id = "findAssetsBeanByUnitName", value = { @Result(property = "id", column = "id"),
//			@Result(property = "builtDate", column = "built_date"), @Result(property = "name", column = "name"),
//			@Result(property = "unit", column = "unit"), @Result(property = "user", column = "user"),
//			@Result(property = "value", column = "value") })
	List<AssetsBean> findAssetsBeanByUnitName(String unit);
}
