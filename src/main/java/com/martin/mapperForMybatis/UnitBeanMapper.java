package com.martin.mapperForMybatis;

import java.util.List;

import com.martin.model.bean.UnitBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UnitBeanMapper {
	
	/**
	 * 找所有部門
	 * @return 部門或沒有東西
	 */
	List<UnitBean> findAllUnitBean();
	
	/**
	 * 由部門名稱尋找部門
	 * @param unitName 部門名稱
	 * @return 特定部門
	 */
	UnitBean findUnitByUnitName(String unitName);
	
	/**
	 * 新增部門
	 * @param unitBean 新增部門
	 * @return 成功的筆數
	 */
	Integer createUnitBean(UnitBean unitBean);
	
	/**
	 * 刪除特定部門
	 * @param unitName 部門名稱
	 */
	void deleteUnitBean(String unitName);
}
