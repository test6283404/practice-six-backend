package com.martin.mapperForMybatis;

import com.martin.model.bean.UserBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserBeanMapper {
	
	/**
	 * 透過使用者id找使用者
	 * @param userId 使用者id
	 * @return 特定使用者
	 */
	UserBean findUserBeanByUserId(Integer userId);
	
	/**
	 * 新增使用者
	 * @param userBean 新增使用者
	 * @return 成功的筆數
	 */
	Integer creatUserBean(UserBean userBean);
	
	/**
	 * 透過使用者帳號找使用者
	 * @param username 使用者帳號
	 * @return 特定使用者
	 */
	UserBean findUserBeanByUserName(String username);
}
