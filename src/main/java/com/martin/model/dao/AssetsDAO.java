package com.martin.model.dao;

import com.martin.model.bean.AssetsBean;

import java.util.List;
import java.util.Map;

public interface AssetsDAO {
    List<AssetsBean> getAll();

    AssetsBean findByAssetsId(Integer id);

    int create(AssetsBean assetsBean);

    int update(Integer id, AssetsBean assetsBean);

    void delete(Integer id);
    
    List<AssetsBean> findByUnit(String unit);
    
    List<Map<String, Object>> findByUnitName(String unit);
}
