package com.martin.model.dao;

import java.util.List;

import com.martin.model.bean.UnitBean;

public interface UnitDAO {
	List<UnitBean> getAll();
	
	int create(UnitBean unitBean);
	
	UnitBean findByUnitName(String unitName);
	
	void delete(String unitName);
}
