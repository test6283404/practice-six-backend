package com.martin.model.dao;

import java.util.List;

import com.martin.model.bean.UnitBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.martin.mapper.UnitMapper;

@Repository
public class UnitDAOImpl implements UnitDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<UnitBean> getAll() {
		String sql = "SELECT * FROM unit";

        return jdbcTemplate.query(sql, new UnitMapper());
	}
	
	@Override
	public int create(UnitBean unitBean) {
		String sql = "insert into unit (unitName) values (?)";

		return jdbcTemplate.update(sql, unitBean.getUnitName());
	}

	@Override
	public UnitBean findByUnitName(String unitName) {
		String sql = "select * from unit where unitName = ?";

		UnitBean unitBean = jdbcTemplate.queryForObject(sql, new UnitMapper(), new Object[] { unitName });
		
		return unitBean;
	}

	@Override
	public void delete(String unitName) {
		 jdbcTemplate.update("delete from unit where unitName =?", unitName);
	}

}
