package com.martin.model.dao;

import com.martin.model.bean.UserBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.martin.mapper.UserMapper;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public UserBean findByUserId(Integer userId) {
		String sql = "select * from user where userId = ?";

		UserBean userBean = jdbcTemplate.queryForObject(sql, new UserMapper(), new Object[] { userId });

		return userBean;
	}

	@Override
	public int create(UserBean userBean) {
		String sql = "insert into user (username, password) values(?, ?)";

		return jdbcTemplate.update(
				sql
				, userBean.getUsername(), userBean.getPassword());
	}

	@Override
	public UserBean findByUsername(String username) {
		String sql = "select * from user where username = ?";

		UserBean userBean = jdbcTemplate.queryForObject(sql, new UserMapper(), new Object[] { username });
		
		return userBean;
	}

}
