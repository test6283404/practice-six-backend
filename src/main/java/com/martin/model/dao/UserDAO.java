package com.martin.model.dao;

import com.martin.model.bean.UserBean;

public interface UserDAO {
	UserBean findByUserId(Integer userId);

	int create(UserBean userBean);
	
	UserBean findByUsername(String username);
}
