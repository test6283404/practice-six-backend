package com.martin.model.dao;

import com.martin.mapper.AssetsMapper;
import com.martin.model.bean.AssetsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class AssetsDAOImpl implements AssetsDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<AssetsBean> getAll() {
		String sql = "SELECT * FROM assets";

		return jdbcTemplate.query(sql, new AssetsMapper());
	}

	@Override
	public AssetsBean findByAssetsId(Integer id) {
		String sql = "select * from assets where id = ?";

		AssetsBean assetsBean = jdbcTemplate.queryForObject(sql, new AssetsMapper(), new Object[] { id });

		return assetsBean;
	}

	@Override
	public int create(AssetsBean assetsBean) {
		String sql = "insert into assets (built_date, name, unit, user, value) values(?, ?, ?, ?, ?)";

		return jdbcTemplate.update(sql, assetsBean.getBuiltDate(), assetsBean.getName(), assetsBean.getUnit(), assetsBean.getUser(),
				assetsBean.getValue());

	}

	@Override
	public int update(Integer id, AssetsBean assetsBean) {
		String sql = "update assets set built_date = ?, name = ?, unit = ?, user = ?, value = ? where id = ?";

		return jdbcTemplate.update(sql, assetsBean.getBuiltDate(), assetsBean.getName(), assetsBean.getUnit(), assetsBean.getUser(),
				assetsBean.getValue(), id);
	}

	@Override
	public void delete(Integer id) {
		jdbcTemplate.update("delete from assets where id =?", id);
	}

	@Override
	public List<AssetsBean> findByUnit(String unit) {
		
		System.out.println("================Enter Dao");
		
		List<AssetsBean> list = new ArrayList<>();
		
		System.out.println("1111111111111111111");
		
		String sql = "select * from assets where unit = ?";

		List<Map<String, Object>> lists = jdbcTemplate.queryForList(sql, new AssetsMapper(), new Object[] { unit });
		
		System.out.println("==========================");
		
		for (Map<String, Object> map : lists) {
			
			System.out.println("222222222222222222222");
			
			for (String key : map.keySet()) {
				list.add((AssetsBean)map.get(key));
			}
		}
		
		return list;
	}

	@Override
	public List<Map<String, Object>> findByUnitName(String unit) {		
		String sql = "SELECT * FROM assets where unit =?;";
		
		List<Map<String, Object>> queryForList = jdbcTemplate.queryForList(sql, unit);
		
		System.out.println(queryForList);
		
		return queryForList;
	}
}
