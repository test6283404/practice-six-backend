package com.martin.model.dto;

public class LoginUserDTO {
	private Integer userId;
	
	private String username;
	
	private boolean isAuthentic;

	
	public LoginUserDTO() {
	}

	public LoginUserDTO(Integer userId, String username, boolean isAuthentic) {
		super();
		this.userId = userId;
		this.username = username;
		this.isAuthentic = isAuthentic;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isAuthentic() {
		return isAuthentic;
	}

	public void setAuthentic(boolean isAuthentic) {
		this.isAuthentic = isAuthentic;
	}

	
}
