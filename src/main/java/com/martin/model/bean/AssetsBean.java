package com.martin.model.bean;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@Table(name = "assets")
@ApiModel(value = "資產物件", description = "資產內部訊息")
public class AssetsBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
	@ApiModelProperty(value = "財產編號")
    private Integer id;

    @Column(name = "built_date")
	@ApiModelProperty(value = "資產建立日期", notes = "格式為 : yyyy-MM-dd")
    private String builtDate;

    @Column(name = "name")
	@ApiModelProperty(value = "財產名稱")
    private String name;

    @Column(name = "unit")
	@ApiModelProperty(value = "使用單位")
    private String unit;

    @Column(name = "user")
	@ApiModelProperty(value = "使用人")
    private String user;

    @Column(name = "value")
    @ApiModelProperty(value = "資產價值")
    private Double value;

    public AssetsBean() {
    }

    public AssetsBean(String builtDate, String name, String unit, String user, Double value) {
        this.builtDate = builtDate;
        this.name = name;
        this.unit = unit;
        this.user = user;
        this.value = value;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBuiltDate() {
        return builtDate;
    }

    public void setBuiltDate(String builtDate) {
        this.builtDate = builtDate;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }
}
