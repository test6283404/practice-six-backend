package com.martin.model.bean;

import javax.persistence.*;

@Entity
@Table(name = "unit")
public class UnitBean {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "unitName")
	private String unitName;

	public String getUnitName() {
		return unitName;
	}

	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}
	
	
}
