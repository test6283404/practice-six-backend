package com.martin.controller;

import java.util.List;

import com.martin.model.bean.UnitBean;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.service.UnitService;

@RestController
@Api(tags = "Unit CRUD")
public class UnitController {
	
	@Autowired
	private UnitService unitService;
	
	Logger log = LoggerFactory.getLogger(getClass()); 
	
	/**
	 * 找到所有使用單位
	 * @return
	 */
	@ApiOperation(value = "找到所有使用單位")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/unit/findAll")
	public ResponseEntity<List<UnitBean>> findAll() {
		log.info("用 取得所有使用單位資料 api ");
		
		try {
			List<UnitBean> unitBeanList = unitService.findAll();

			if (!unitBeanList.isEmpty()) {
				log.info("使用單位取得成功");
				
				return new ResponseEntity<>(unitBeanList, HttpStatus.OK);
			} else {
				log.error("使用單位取得失敗");
				
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("使用單位取得失敗");
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 新增一筆使用單位的資料
	 * @param unitBean
	 * @return
	 */
	@ApiOperation(value = "新增一筆使用單位的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/unit/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true,value = "新增一筆使用單位資料") @RequestBody UnitBean unitBean) {
		log.info("用 新增一筆使用單位資料 api ");
		
		try {
			if (unitBean.getUnitName() != "") {
				System.out.println("controller=====================");
				boolean create = unitService.addUnit(unitBean);
				System.out.println(create + "=====================");
				if (create) {
					log.info("新增使用單位資料成功");
					
					return new ResponseEntity<>("Success", HttpStatus.OK);
				} else {
					log.error("使用單位資料新增失敗");
					
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				}
			}
			
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			System.out.println(e + "========================");
			log.error("使用單位資料新增失敗");
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * 刪除特定名稱的使用單位資料
	 * @param unitBean
	 * @return
	 */
	@ApiOperation(value = "刪除特定名稱的使用單位資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/unit/delete")
	public ResponseEntity<Void> delete(
			@ApiParam(required = true,value = "刪除特定名稱的資料") @RequestBody UnitBean unitBean) {
		log.info("用 刪除特定名稱的資料 api ");
		
		try {
			unitService.delete(unitBean.getUnitName());

			log.info("刪除成功");
			
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("失敗");
			
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
