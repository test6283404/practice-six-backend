package com.martin.controller;

import com.martin.model.bean.AssetsBean;
import com.martin.model.bean.UnitBean;
import com.martin.model.dto.ForCreateAndUpdateDTO;
import com.martin.model.dto.ForIdDTO;
import com.martin.service.AssetsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@Api(tags = "Assets CRUD")
public class AssetsController {

	@Autowired
	private AssetsService assetsService;

	Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * 取得所有資料
	 * 
	 * @return ResponseEntity<List<Assets>>
	 */
	@ApiOperation(value = "取得所有資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/findAll")
	public ResponseEntity<List<AssetsBean>> findAll() {
		log.info("用 取得所有資料 api ");

		try {
			List<AssetsBean> assetsBeanList = assetsService.findAll();

			if (!assetsBeanList.isEmpty()) {
				log.info("取得成功");

				return new ResponseEntity<>(assetsBeanList, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 取得特定id的資料
	 * 
	 * @param id
	 * @return Assets
	 */
	@ApiOperation(value = "取得特定id的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/findById")
	public ResponseEntity<AssetsBean> findById(@ApiParam(required = true, value = "請傳入財產編號") @RequestBody ForIdDTO forIdDTO) {
		log.info("用 取得特定id的資料 api ");

		try {
			AssetsBean assetsBean = assetsService.findById(forIdDTO.getId());

			if (assetsBean != null) {
				log.info("取得成功");

				return new ResponseEntity<>(assetsBean, HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 新增一筆資料
	 *
	 * @return Assets
	 */
	@ApiOperation(value = "新增一筆資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/create")
	public ResponseEntity<String> create(
			@ApiParam(required = true, value = "新增一筆資料，時間格式為 : yyyy-MM-dd") @RequestBody ForCreateAndUpdateDTO fCreateAndUpdate) {
		log.info("用 新增一筆資料 api ");

		try {
			AssetsBean assetsBean = new AssetsBean(fCreateAndUpdate.getBuiltDate(), fCreateAndUpdate.getName(),
					fCreateAndUpdate.getUnit(), fCreateAndUpdate.getUser(), fCreateAndUpdate.getValue());

			boolean create = assetsService.create(assetsBean);

			if (create) {
				log.info("新增成功");

				return new ResponseEntity<>("Success", HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 更新一筆資料
	 * 
	 * @return
	 */
	@ApiOperation(value = "更新一筆資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/update")
	public ResponseEntity<String> update(@ApiParam(required = true, value = "更新一筆資料") @RequestBody AssetsBean inputAssetsBean) {
		log.info("用 更新一筆資料 api ");

		try {
			AssetsBean assetsBean = new AssetsBean(inputAssetsBean.getBuiltDate(), inputAssetsBean.getName(), inputAssetsBean.getUnit(),
					inputAssetsBean.getUser(), inputAssetsBean.getValue());

			boolean update = assetsService.update(inputAssetsBean.getId(), assetsBean);

			if (update) {
				log.info("更新成功");

				return new ResponseEntity<>("Success", HttpStatus.OK);
			} else {
				log.error("失敗");

				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 刪除特定id的資料
	 * 
	 * @return
	 */
	@ApiOperation(value = "刪除特定id的資料")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/delete")
	public ResponseEntity<Void> delete(@ApiParam(required = true, value = "刪除特定id的資料") @RequestBody ForIdDTO forIdDTO) {
		log.info("用 刪除特定id的資料 api ");

		try {
			assetsService.delete(forIdDTO.getId());

			log.info("刪除成功");

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 產生Excel報表
	 * 
	 * @param unitBean
	 * @return
	 */
	@ApiOperation(value = "產生Excel報表")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/createExcel")
	public ResponseEntity<String> createExcel(
			@ApiParam(required = true, value = "用UnitName取得excel檔") @RequestBody UnitBean unitBean) {
		log.info("使用 產生Excel報表 api");

		try {
			assetsService.createExcel(unitBean.getUnitName());

			log.info("建立excel成功");

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("建立excel失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * 匯入csv檔
	 * 
	 * @param file
	 * @return
	 * @throws IOException 
	 */
	@ApiOperation(value = "匯入csv檔")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/readCSV")
	public ResponseEntity<String> readCSV(@RequestBody MultipartFile file) {
		log.info("使用 匯入csv檔 api");

		try {
			assetsService.addCSV(file);

			log.info("匯入csv檔成功");

			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			log.error("匯入csv檔失敗");

			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
