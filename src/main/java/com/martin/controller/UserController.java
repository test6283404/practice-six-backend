package com.martin.controller;

import javax.servlet.http.HttpSession;

import com.martin.model.bean.UserBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.martin.model.dto.UsernameAndPasswordDTO;
import com.martin.model.dto.LoginUserDTO;
import com.martin.service.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags = "User CRUD")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	Logger log = LoggerFactory.getLogger(getClass()); 
	
	/**
	 * User註冊
	 * @param nameAndPasswordDto
	 * @return
	 */
	@ApiOperation(value = "User註冊")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 404, message = "沒任何東西"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/user/create")
	public ResponseEntity<String> createUser(@ApiParam(required = true, value = "Username And Password") @RequestBody UsernameAndPasswordDTO nameAndPasswordDto) {
		boolean isExist = userService.checkIfUsernameExist(nameAndPasswordDto.getUsername());
		
		if (isExist) {
			log.error("帳號已被註冊");
			
			return new ResponseEntity<>("帳號已被註冊", HttpStatus.BAD_REQUEST);
		} else {
			UserBean userBean = new UserBean();
			
			userBean.setUsername(nameAndPasswordDto.getUsername());
			userBean.setPassword(nameAndPasswordDto.getPassword());
			
			userService.addUser(userBean);
			
			log.info("註冊成功");
			
			return new ResponseEntity<String>("註冊成功", HttpStatus.OK);
		}
		
	}
	
	/**
	 * User登入
	 * @param nameAndPasswordDto
	 * @param session
	 * @return
	 */
	@ApiOperation(value = "User登入")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/user/login")
	public ResponseEntity<?> login(@ApiParam(required = true, value = "Username, Password and Session") @RequestBody UsernameAndPasswordDTO nameAndPasswordDto, HttpSession session) {
		UserBean userBean = userService.checkLogin(nameAndPasswordDto.getUsername(), nameAndPasswordDto.getPassword());
		
		if (userBean != null) {
			LoginUserDTO loginUser = new LoginUserDTO(userBean.getUserId(), userBean.getUsername(), true);
			
			session.setAttribute("loginUser", loginUser);
			
			log.info("登入成功");
			
			return new ResponseEntity<String>("登入成功", HttpStatus.OK);
		}
		
		log.error("登入失敗");
		
		return new ResponseEntity<String>("登入失敗", HttpStatus.UNAUTHORIZED);
	}
	
	/**
	 * User登出
	 * @param session
	 * @return
	 */
	@ApiOperation(value = "User登出")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/user/logout")
	public ResponseEntity<?> logout(@ApiParam(required = true, value = "Session only") HttpSession session) {
		session.removeAttribute("loginUser");
		
		log.info("登出!");
		
		return new ResponseEntity<String>("登出!", HttpStatus.OK);
	}

	/**
	 * 確認session有沒有存登入訊息
	 * @param httpSession
	 * @return
	 */
	@ApiOperation(value = "確認session有沒有存登入訊息")
	@ApiResponses({ @ApiResponse(code = 200, message = "success"), @ApiResponse(code = 401, message = "登入失敗"),
			@ApiResponse(code = 400, message = "你輸入錯的東西") })
	@PostMapping("/users/map")
	public ResponseEntity<?> testSessionValue(@ApiParam(required = true, value = "Session only") HttpSession httpSession){

		log.info("登入檢查");

		LoginUserDTO loginUser = (LoginUserDTO) httpSession.getAttribute("loginUser");

		if(loginUser == null){
			log.error("session attribute 空的");
			return new ResponseEntity<String>("session attribute null", HttpStatus.UNAUTHORIZED); // 401
		}

		Map<String,String> responseMap =  new HashMap<>();

		responseMap.put("userId",loginUser.getUserId().toString());
		responseMap.put("userName",loginUser.getUsername());

		return new ResponseEntity<Map<String,String>>(responseMap, HttpStatus.OK);
	}
}
