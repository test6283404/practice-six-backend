package com.martin.mapper;

import com.martin.model.bean.AssetsBean;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AssetsMapper implements RowMapper<AssetsBean> {

    @Override
    public AssetsBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        AssetsBean assetsBean = new AssetsBean();

        assetsBean.setId(rs.getInt("id"));
        assetsBean.setBuiltDate(rs.getString("built_date"));
        assetsBean.setName(rs.getString("name"));
        assetsBean.setUnit(rs.getString("unit"));
        assetsBean.setUser(rs.getString("user"));
        assetsBean.setValue(rs.getDouble("value"));

        return assetsBean;
    }
}
