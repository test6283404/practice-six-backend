package com.martin.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.martin.model.bean.UnitBean;
import org.springframework.jdbc.core.RowMapper;

public class UnitMapper implements RowMapper<UnitBean>{

	@Override
	public UnitBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		UnitBean unitBean = new UnitBean();
		
		unitBean.setUnitName(rs.getString("unitName"));
		
		return unitBean;
	}
	
}
