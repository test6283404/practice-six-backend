package com.martin.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.martin.model.bean.UserBean;
import org.springframework.jdbc.core.RowMapper;

public class UserMapper implements RowMapper<UserBean> {

	@Override
	public UserBean mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserBean userBean = new UserBean();
		
		userBean.setUserId(rs.getInt("userId"));
		userBean.setUsername(rs.getString("username"));
		userBean.setPassword(rs.getString("password"));
		
		return userBean;
	}

}
