package com.martin.service;

import com.martin.model.bean.UserBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.mapperForMybatis.UserBeanMapper;
import com.martin.model.dao.UserDAOImpl;

@Service
public class UserService {
	
	@Autowired
	private UserDAOImpl userDaoImpl;
	
	@Autowired
	private UserBeanMapper userBeanMapper;
	
	/**
	 * 新增使用者
	 * @param userBean 新增使用者
	 * @return 成功是true
	 */
	public boolean addUser(UserBean userBean) {
//		return userDaoImpl.create(user) == 1;
		userBeanMapper.creatUserBean(userBean);
		
		return true;
	}
	
	public boolean checkIfUsernameExist(String username) {
		
		try {
//			User user = userDaoImpl.findByUsername(username);
			
			UserBean userBean = userBeanMapper.findUserBeanByUserName(username);
			
			if (userBean != null) {
				return true;
			}
			
			return false;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 檢查使用者的登入合法性
	 * @param username 帳號
	 * @param password 密碼
	 * @return 合格使用者
	 */
	public UserBean checkLogin(String username, String password) {
		try {
//			User user = userDaoImpl.findByUsername(username);
			
			UserBean userBean = userBeanMapper.findUserBeanByUserName(username);
			
			if (userBean.getPassword().equals(password)) {
				return userBean;
			}
			
			return null;
		} catch (Exception e) {
			return null;
		}
	}
}
