package com.martin.service;

import java.util.List;

import com.martin.model.bean.UnitBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.martin.mapperForMybatis.UnitBeanMapper;
import com.martin.model.dao.UnitDAOImpl;

@Service
public class UnitService {

	@Autowired
	private UnitDAOImpl unitDaoImpl;

	@Autowired
	private UnitBeanMapper unitBeanMapper;

	/**
	 * 尋找所有部門
	 * @return 部門或沒有東西
	 */
	public List<UnitBean> findAll() {
//		return unitDaoImpl.getAll();
		return unitBeanMapper.findAllUnitBean();
	}

	/**
	 * 新增部門
	 * @param unitBean 新增部門
	 * @return 成功是true
	 */
	public boolean addUnit(UnitBean unitBean) {
//		return unitDaoImpl.create(unit) == 1;
		unitBeanMapper.createUnitBean(unitBean);
		
		return true;
	}

	/**
	 * 由部門名稱尋找部門
	 * @param unitName 部門名稱
	 * @return 特定部門
	 */
	public UnitBean findUnitByUnitName(String unitName) {

		try {
//			return unitDaoImpl.findByUnitName(unitName);
			return unitBeanMapper.findUnitByUnitName(unitName);
		} catch (Exception e) {
			return null;
		}

	}

	/**
	 * 刪除特定部門
	 * @param unitName 部門名稱
	 */
	public void delete(String unitName) {
//		unitDaoImpl.delete(unitName);
		unitBeanMapper.deleteUnitBean(unitName);
	}
}
