package com.martin.service;

import com.martin.mapperForMybatis.AssetsBeanMapper;
import com.martin.model.bean.AssetsBean;
import com.martin.model.dao.AssetsDAOImpl;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 處理 Assets 的邏輯判斷
 */
@Service
public class AssetsService {

	@Autowired
	private AssetsDAOImpl assetsDao;

	@Autowired
	private AssetsBeanMapper assetsBeanMapper;

	/**
	 * 找出所有 Assets
	 * 
	 * @return List<Assets> 有東西或是null
	 */
	public List<AssetsBean> findAll() {
//		return assetsDao.getAll();
		return assetsBeanMapper.findAllAssetsBean();
	}

	/**
	 * 找特定的 Assets
	 * 
	 * @param id assets的id
	 * @return Assets 有東西或是null
	 */
	public AssetsBean findById(Integer id) {
//		return assetsDao.findByAssetsId(id);
		return assetsBeanMapper.findAssetsBeanByAssetsBeanId(id);
	}

	/**
	 * 新增一筆資料
	 * 
	 * @param assetsBean 傳進新的assets
	 */
	public boolean create(AssetsBean assetsBean) {
		String builtDate = assetsBean.getBuiltDate();

		if (checkTimeFormat(builtDate)) {
//			return assetsDao.create(assets) == 1;
			return assetsBeanMapper.createAssetsBean(assetsBean) == 1;
		}

		return false;
	}

	/**
	 * 更新一筆資料
	 * 
	 * @param id assets的id 用來找特定assets
	 * @param assetsBean 回傳更新的assets
	 */
	public boolean update(Integer id, AssetsBean assetsBean) {
		String builtDate = assetsBean.getBuiltDate();

		if (checkTimeFormat(builtDate)) {
//			return assetsDao.update(id, assets) == 1;

			AssetsBean assetsBeanByAssetsBeanIdBean = assetsBeanMapper.findAssetsBeanByAssetsBeanId(id);

			if(assetsBeanByAssetsBeanIdBean != null) {
				assetsBeanByAssetsBeanIdBean.setBuiltDate(assetsBean.getBuiltDate());
				assetsBeanByAssetsBeanIdBean.setName(assetsBean.getName());
				assetsBeanByAssetsBeanIdBean.setUnit(assetsBean.getUnit());
				assetsBeanByAssetsBeanIdBean.setUser(assetsBean.getUser());
				assetsBeanByAssetsBeanIdBean.setValue(assetsBean.getValue());

				return assetsBeanMapper.updateAssetsBean(assetsBeanByAssetsBeanIdBean) == 1;
			}
			
		}

		return false;
	}

	/**
	 * 刪除特定資料
	 * 
	 * @param id assets的id
	 */
	public void delete(Integer id) {
//		assetsDao.delete(id);
		assetsBeanMapper.delete(id);
	}

	/**
	 * 對傳入的時間進行正規判斷
	 * 
	 * @param date assets傳進的時間
	 * @return boolean 正確是true
	 */
	private boolean checkTimeFormat(String date) {
		String regex = "^\\d{4}[\\-/\\.](0?[1-9]|1[012])[\\-/\\.](0?[1-9]|[12][0-9]|3[01])$";
		Pattern pattern = Pattern.compile(regex);

		return pattern.matcher(date).find();
	}

	/**
	 *  產生特定單位的資產報表，用Apache poi來製作excel的xlsx的檔案
	 * @param unit 傳入的部門名稱
	 * @throws IOException
	 */
	public void createExcel(String unit) throws IOException {
//		String path = "D:\\User\\D3012203\\999-temp\\";
		int counter = 1;

		List<Map<String, Object>> findByUnitName = assetsDao.findByUnitName(unit);

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("資產報表");

		Map<String, Object[]> data = new TreeMap<String, Object[]>();

		data.put("1", new Object[] { "使用單位", "財產編號", "財產名稱", "資產建立日期", "使用人", "資產價值" });

		for (Map<String, Object> map : findByUnitName) {
			data.put(Integer.toString(counter + 1), new Object[] { map.get("unit"), map.get("id"), map.get("name"),
					map.get("built_date"), map.get("user"), map.get("value") });

			counter++;
		}

		Set<String> keyset = data.keySet();
		int rownumber = 0;

		for (String key : keyset) {
			Row row = sheet.createRow(rownumber++);

			System.out.println(key);

			Object[] objectArray = data.get(key);

			int cellnumber = 0;

			for (Object object : objectArray) {
				Cell cell = row.createCell(cellnumber++);

				if (object instanceof String) {
					cell.setCellValue((String) object);
				} else if (object instanceof Integer) {
					cell.setCellValue((Integer) object);
				} else if (object instanceof Double) {
					cell.setCellValue((Double) object);
				}
			}

		}

		FileOutputStream fileOutputStream = new FileOutputStream("src/main/resources/xlsx/資產報表" + "_" + unit + ".xlsx");
		workbook.write(fileOutputStream);

		fileOutputStream.close();
		workbook.close();

	}

	/**
	 * 傳入CSV檔案，將內容送進assets中
	 * @param file
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void addCSV(MultipartFile file) throws IOException, FileNotFoundException {
		InputStream inputStream = new BufferedInputStream(file.getInputStream());
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		String temp;

		while ((temp = bufferedReader.readLine()) != null) {
			String[] stringArray = temp.split(",");

			AssetsBean assetsBean = new AssetsBean();

			assetsBean.setBuiltDate(stringArray[0]);
			assetsBean.setName(stringArray[1]);
			assetsBean.setUnit(stringArray[2]);
			assetsBean.setUser(stringArray[3]);
			assetsBean.setValue(Double.valueOf(stringArray[4]));

			assetsDao.create(assetsBean);
		}

		bufferedReader.close();

	}
}
