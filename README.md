程式的使用說明書-後端
===

程式概述
---
能夠 註冊 登入 資產及使用單位 crud

程式結構說明
---
以 MVC 架構完成，分別為: Controller, Service, Model(Bean, DAO, DTO), Config, Mapper.

Config 部分是:

    1. 我寫了 SpringFox 的配置，使用 SWAGGER_2 
    2. Cors 的配置

Controller 部分是:

    1. Assets 有基本crud、產生excel報表、讀取csv並寫入
    2. Unit 有基本crud
    3. User 註冊、登入、登出、檢查session內容

Service 部分是:

    1. api 主要的邏輯。
    2. assetsBean 使用 apache poi 生產excel，用bufferedInputStream 讀取 .csv

Bean 部分是:

    1. 和 mssql 連動，綁定 assets 資料庫上

DAO 部分是:

    1. 使用 jdbc template 的配置，方便 Service 調用
    2. DAOImpl 實作 DAO 的方法

DTO 部分是:

    1. ForIdDTO 只有 Integer
    2. ForCreateAndUpdateDTO 包含 Assets 的屬性，除了 id

Mapper :

    1. 各自實作RowMapper，方便jdbcTemplate.query... 來使用

操作說明
---

**自行建置jar檔**

PowerShell 執行

   1. 先cd到專案所在目錄
      1. mvn compile
      2. mvn package
   2. 再cd到專案中的子目錄 '<你的專案位置>\target'
      1. java -jar spring-boot-assets-crud-0.0.1-SNAPSHOT.jar

注意事項
---
JDK 版本配置

`java version "11"`

SpringBoot 版本配置

`SpringBoot 2.5.6`